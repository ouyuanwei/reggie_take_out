package com.itheima.reggie.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Page<T> {
    protected List<T> records;
    protected long total;
    protected long pageSize;
    protected long page;
    protected long pages;
    public Page(List<T> records, long total, long pageSize, long page) {
        this.records = records;
        this.total = total;
        this.pageSize = pageSize;
        this.page = page;
    }
}
