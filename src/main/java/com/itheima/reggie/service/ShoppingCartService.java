package com.itheima.reggie.service;

import com.itheima.reggie.commom.R;
import com.itheima.reggie.entity.ShoppingCart;

import java.util.List;

public interface ShoppingCartService {
    ShoppingCart add(ShoppingCart shoppingCart);

    List<ShoppingCart> findCartByUserId(Long user);

    void clean(Long user);


    void singleClean(Long user, ShoppingCart shoppingCart);
}
