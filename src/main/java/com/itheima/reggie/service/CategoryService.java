package com.itheima.reggie.service;

import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Page;

import java.util.List;

public interface CategoryService {

    void sava(Category category);

    Page<Category> findByPage(Integer page, Integer pageSite);

    void removeBtId(long id);

    void updateById(Category category);

    List<Category> list(Integer type);
}
