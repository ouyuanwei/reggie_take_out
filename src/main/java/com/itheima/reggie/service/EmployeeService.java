package com.itheima.reggie.service;

import com.itheima.reggie.commom.R;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.entity.Page;

public interface EmployeeService {

    R<Employee> login(Employee employee);

    void save(Employee employee);


    Page<Employee> findByPage(Integer page, Integer pageSize, String name);

    void update(Employee employee);

    Employee findById(long id);
}

