package com.itheima.reggie.service;

import com.itheima.reggie.dto.OrderDto;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.entity.Page;

public interface OrderService {
    void submit(Orders orders, Long user);
    Page<OrderDto> page(Integer page, Integer pageSize, Long user);

    Page<OrderDto> page2(Integer page, Integer pageSize, String name, String begindate, String date);
}
