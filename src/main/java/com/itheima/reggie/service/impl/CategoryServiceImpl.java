package com.itheima.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.reggie.commom.CustomException;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.mapper.CateGoryMapper;
import com.itheima.reggie.mapper.DishMapper;
import com.itheima.reggie.mapper.SetMealMapper;
import com.itheima.reggie.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired(required = false)
    private CateGoryMapper cateGoryMapper;
    @Autowired(required = false)
    private DishMapper dishMapper;

    @Autowired(required = false)
    private SetMealMapper setMealMapper;
    //保存类别
    @Override
    public void sava(Category category) {
   cateGoryMapper.save(category);
    }

    @Override
    public Page<Category> findByPage(Integer page, Integer pageSize) {
        PageHelper.startPage(page,pageSize);
        List<Category> categoryList=cateGoryMapper.findAll();
        PageInfo<Category> PageInfo = new PageInfo<>(categoryList);
        Page<Category>  Page = new Page<>(PageInfo.getList(),PageInfo.getTotal(),page,pageSize);
        return Page;
    }

    @Override
    public void removeBtId(long id) {
        long count=dishMapper.findDishCountByCategoryId(id);
        if(count>0){
            throw  new CustomException("该类别关联了菜品，无法删除");
        }
        count=setMealMapper.findSetMealByCategoryId(id);
        if (count>0){
            throw  new CustomException("该类别关联了套餐，无法删除");
        }

        cateGoryMapper.deletById(id);
    }

    @Override
    public void updateById(Category category) {
        category.setUpdateTime(LocalDateTime.now());
        cateGoryMapper.updateById(category);
    }

    @Override
    public List<Category> list(Integer type) {
       return cateGoryMapper.list(type);
    }
}
