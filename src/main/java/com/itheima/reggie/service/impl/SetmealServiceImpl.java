package com.itheima.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.reggie.commom.CustomException;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.mapper.CateGoryMapper;
import com.itheima.reggie.mapper.SetMealMapper;
import com.itheima.reggie.mapper.SetmealDishMapper;
import com.itheima.reggie.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class SetmealServiceImpl implements SetmealService {
    @Autowired(required = false)
    private SetMealMapper setmealMapper;

    @Autowired(required = false)
    private SetmealDishMapper setmealDishMapper;
    @Autowired(required = false)
    private CateGoryMapper cateGoryMapper;
    @Override
    @CacheEvict(value = "setmealCahe",allEntries = true)
    public void save(SetmealDto setmealDto) {
        setmealDto.setCreateTime(LocalDateTime.now());
        setmealDto.setUpdateTime(LocalDateTime.now());

        setmealMapper.save(setmealDto);


        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(setmealDto.getId());
            setmealDish.setCreateUser(setmealDto.getCreateUser());
            setmealDish.setUpdateUser(setmealDto.getUpdateUser());
            setmealDish.setCreateTime(LocalDateTime.now());
            setmealDish.setUpdateTime(LocalDateTime.now());
            setmealDish.setSort(0);
        }
        setmealDishMapper.saveBatch(setmealDishes);

    }

    @Override
    @Transactional
    public Page<SetmealDto> findByPage(Integer page, Integer pageSize, String name) {
        PageHelper.startPage(page, pageSize);
        List<Setmeal> setmealList = setmealMapper.findByname(name);
        PageInfo<Setmeal> setmealPageInfo = new PageInfo<>(setmealList);
        List<SetmealDto> setmealDtoList = new ArrayList<>();
        for (Setmeal setmeal : setmealPageInfo.getList()) {
            SetmealDto setmealDto = new SetmealDto();
            BeanUtils.copyProperties(setmeal, setmealDto);
            Category category = cateGoryMapper.findById(setmeal.getCategoryId());
            setmealDto.setCategoryName(category.getName());
            setmealDtoList.add(setmealDto);
        }
        Page<SetmealDto> pageResult = new Page<>(setmealDtoList,setmealPageInfo.getTotal(),page,pageSize);
        return  pageResult;
    }
    @Override
    @Transactional
    @CacheEvict(value = "setmealCahe",allEntries = true)
    public void deleteById(List<Long> ids) {
   Long count=setmealMapper.queryDishWithStatus(ids);
    if(count>0){
        throw  new CustomException("存在启售，无法删除");
        }
    setmealMapper.deletByIds(ids);
    setmealDishMapper.deleteBySetmealId(ids);

    }
    /*
     * 根据套餐的类别展示套餐
     */
    @Override
    @Cacheable(value = "setmealCahe",key = "#categoryId+'_'+#status")
    public List<Setmeal> list(Long categoryId, Integer status) {

        return setmealDishMapper.list(categoryId,status);
    }

    @Override
    public void updateSetmealStart(Integer status, List<Long> ids) {
        setmealMapper.updateSetmealStart(status,ids);
    }

    @Override
    public SetmealDto updatefind(Long id) {
            Setmeal setmeal=setmealMapper.findsetmeal(id);
        List<SetmealDish> setmealDishes=setmealDishMapper.finsetmealdish(id);
        SetmealDto setmealDto = new SetmealDto();
        BeanUtils.copyProperties(setmeal,setmealDto);
         setmealDto.setSetmealDishes(setmealDishes);
        return setmealDto;
    }

    @Override
    public void updatefind2(SetmealDto setmealDto) {
        setmealMapper.updatefind2(setmealDto);
        Long id = setmealDto.getId();
         setmealDishMapper.deletedishone(id);
        List<SetmealDish> setmealDishes=setmealDto.getSetmealDishes();
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(setmealDto.getId());
            setmealDish.setCreateUser(setmealDto.getCreateUser());
            setmealDish.setCreateTime(LocalDateTime.now());
            setmealDish.setUpdateTime(LocalDateTime.now());
            setmealDish.setUpdateUser(setmealDto.getUpdateUser());
            setmealDish.setSort(0);
        }
        setmealDishMapper.saveBatch(setmealDishes);
    }
}



