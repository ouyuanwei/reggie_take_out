package com.itheima.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.reggie.dto.OrderDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.*;
import com.itheima.reggie.mapper.*;
import com.itheima.reggie.service.OrderService;
import com.itheima.reggie.util.UUIDUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired(required = false)
    private OrderMapper orderMapper;

    @Autowired(required = false)
    private OrderDetailMapper orderDetailMapper;


    @Autowired(required = false)
    private ShoppingCartMapper shoppingCartMapper;

    @Autowired(required = false)
    private UserMapper userMapper;

    @Autowired(required = false)
    private AddressBookMapper addressBookMapper;

    @Override
    public void submit(Orders orders, Long user) {
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.findcartByUserId(user);

         User user1=userMapper.findById(user);

        AddressBook byId = addressBookMapper.getById(orders.getAddressBookId());

         Long orderId= UUIDUtils.getUUIDInOrderId().longValue();

        BigDecimal bigDecimal = new BigDecimal("0");
        List<OrderDetail> orderDetails = new ArrayList<>();
        for (ShoppingCart shoppingCart : shoppingCarts) {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setId(orderId);
            orderDetail.setName(shoppingCart.getName());
            orderDetail.setImage(shoppingCart.getImage());
            orderDetail.setOrderId(orderId);
            orderDetail.setDishId(shoppingCart.getDishId());
            orderDetail.setSetmealId(shoppingCart.getSetmealId());
            orderDetail.setDishFlavor(shoppingCart.getDishFlavor());
            orderDetail.setNumber(shoppingCart.getNumber());
            orderDetail.setAmount(shoppingCart.getAmount());
            BigDecimal amount=shoppingCart.getAmount().multiply(new BigDecimal(shoppingCart.getNumber()));
               bigDecimal=bigDecimal.add(amount);
               orderDetails.add(orderDetail);
        }
        orders.setId(orderId);
        orders.setNumber(String.valueOf(orderId));
        orders.setStatus(1);
        orders.setUserId(user);
        orders.setOrderTime(LocalDateTime.now());
        orders.setCheckoutTime(LocalDateTime.now());
        orders.setAmount(bigDecimal);
        orders.setUserName(user1.getName());
        orders.setPhone(byId.getPhone());
        orders.setAddress(byId.getDetail());
        orders.setConsignee(byId.getConsignee());

    orderMapper.save(orders);
    orderDetailMapper.saverBatch(orderDetails);
     shoppingCartMapper.clean(user);
    }

   //分页查询历史订单和最新订单（实战）
    @Override
    public Page<OrderDto> page(Integer page, Integer pageSize, Long user) {
        PageHelper.startPage(page,pageSize);
        List<Orders> ordersList=orderMapper.findNewOrder(user);
        PageInfo<Orders> pageInfo = new PageInfo<>(ordersList);
        List<OrderDto> orderDtos = new ArrayList<>();
        for (Orders orders : pageInfo.getList()) {
            OrderDto orderDto = new OrderDto();
            BeanUtils.copyProperties(orders,orderDto);
            List<OrderDetail> orderDetails=orderDetailMapper.findNewOrderDetail(orders.getId());
            orderDto.setOrderDetails(orderDetails);
            orderDtos.add(orderDto);
        }
        Page<OrderDto> orderDtoPage = new Page<>(orderDtos,pageInfo.getTotal(),page,pageSize,pageInfo.getPages());

        return orderDtoPage;
    }

    @Override
    public Page<OrderDto> page2(Integer page, Integer pageSize, String number, String beginTime, String endTime) {
        PageHelper.startPage(page,pageSize);
         List<Orders> ordersList=orderMapper.findordername(number,beginTime,endTime);
        PageInfo<Orders> ordersPageInfo = new PageInfo<>(ordersList);
        List<OrderDto> orderDtoList=new ArrayList<>();
        for (Orders orders : ordersPageInfo.getList()) {
            OrderDto orderDto = new OrderDto();
            BeanUtils.copyProperties(orders,orderDto);
            List<OrderDetail> newOrderDetail = orderDetailMapper.findNewOrderDetail(orders.getId());
            orderDto.setOrderDetails(newOrderDetail);
            orderDtoList.add(orderDto);
        }
        Page<OrderDto> pageResult = new Page<>(orderDtoList,ordersPageInfo.getTotal(),page,pageSize);


        return pageResult;
    }
}
