package com.itheima.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.reggie.dto.OrderDto;
import com.itheima.reggie.entity.OrderDetail;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.mapper.OrderDetailMapper;
import com.itheima.reggie.mapper.OrderMapper;
import com.itheima.reggie.mapper.UserMapper;
import com.itheima.reggie.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    private UserMapper userMapper;

    @Autowired(required = false)
    private OrderMapper orderMapper;

    @Autowired(required = false)
    private OrderDetailMapper orderDetailMapper;
    @Override
    public User login(String phone, String code, String codeInSession) {

         User dbUser = null;
        //User dbuser = new User();
        if(codeInSession!=null && code.equalsIgnoreCase(codeInSession)){
           dbUser= userMapper.findByPhone(phone);
           if (dbUser==null){
               dbUser=new User();
               dbUser.setPhone(phone);
               dbUser.setStatus(1);
               userMapper.save(dbUser);
           }
        }


        return dbUser;
    }




}
