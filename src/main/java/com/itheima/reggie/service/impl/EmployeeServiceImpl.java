package com.itheima.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.reggie.commom.R;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.mapper.EmployeeMapper;
import com.itheima.reggie.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired(required = false)
    private EmployeeMapper employeeMapper;

    @Override
    public R<Employee> login(Employee employee) {
        Employee dbEmployee = employeeMapper.login(employee); //根据用户名查找用户
        if(dbEmployee==null){
            return R.error("用户名不存在");
        }
        if(!dbEmployee.getPassword().equals(DigestUtils.md5DigestAsHex(employee.getPassword().getBytes()))){
               return R.error("用户名不存在");



    }
        if(dbEmployee.getStatus()==0){
        R.error("该用户已被禁用");
        }
        return R.success(dbEmployee);
    }

    @Override
    public void save(Employee employee) {
         employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
         employee.setStatus(1);
         employee.setCreateTime(LocalDateTime.now());
         employee.setUpdateTime(LocalDateTime.now());
         employeeMapper.save(employee);

    }

    @Override
    public Page<Employee> findByPage(Integer page, Integer pageSize, String name) {
        //1. 设置当前页与页面大小
        PageHelper.startPage(page,pageSize);

        //2. 查询数据
        List<Employee> employeeList = employeeMapper.findByName(name);

        //3. 创建PageINfo对象，把list集合传入
        PageInfo<Employee> pageInfo = new PageInfo(employeeList);

        //4. 把pageinfo的信息转移到Page对象
        Page<Employee> pageResult = new Page<>(pageInfo.getList(),pageInfo.getTotal(),pageInfo.getPageSize(),pageInfo.getPageNum());


        return pageResult;
    }

    @Override
    public void update(Employee employee) {
        employee.setUpdateTime(LocalDateTime.now());
        employeeMapper.update(employee);
    }

    @Override
    public Employee findById(long id) {
        return employeeMapper.findById(id);
    }


}
