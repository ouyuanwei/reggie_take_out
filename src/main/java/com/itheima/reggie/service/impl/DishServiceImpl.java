package com.itheima.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.reggie.commom.CustomException;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.*;
import com.itheima.reggie.mapper.*;
import com.itheima.reggie.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class DishServiceImpl implements DishService {
    @Autowired(required = false)
    private DishMapper dishMapper;

    @Autowired(required = false)
    private DishFlavorMapper dishFlavorMapper;
    @Autowired(required = false)
    private CateGoryMapper cateGoryMapper;
    @Autowired(required = false)
private SetMealMapper setMealMapper;
    @Autowired(required = false)
    private SetmealDishMapper setmealDishMapper;
    @Autowired(required = false)
    private RedisTemplate redisTemplate;

    @Override
    @Transactional
    public void saveWithFlavor(DishDto dishDto) {
        dishDto.setCreateTime(LocalDateTime.now());
        dishDto.setUpdateTime(LocalDateTime.now());
        dishDto.setSort(0);
        dishMapper.save(dishDto);

        List<DishFlavor> flavors = dishDto.getFlavors();
        for (DishFlavor flavor : flavors) {
            flavor.setDishId(dishDto.getId());
            flavor.setCreateTime(LocalDateTime.now());
            flavor.setUpdateTime(LocalDateTime.now());
            flavor.setCreateUser(dishDto.getCreateUser());
            flavor.setUpdateUser(dishDto.getUpdateUser());
        }
        dishFlavorMapper.saveBatch(flavors);
        Set keys = redisTemplate.keys("dish_*");
        redisTemplate.delete(keys);


    }

    @Override
    public Page<DishDto> page(Integer page, Integer pageSize, String name) {
        PageHelper.startPage(page,pageSize);
        List<Dish> dishList=dishMapper.findByName(name);
        PageInfo<Dish> pageInfo = new PageInfo<>(dishList);
        List<DishDto> dishDtoList = new ArrayList<>();
        for (Dish dish : pageInfo.getList()) {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(dish,dishDto);
            Category category=cateGoryMapper.findById(dish.getCategoryId());
            dishDto.setCategoryName(category.getName());
            dishDtoList.add(dishDto);
        }
        Page<DishDto> dishDtoPage = new Page<>(dishDtoList,pageInfo.getTotal(),pageSize,page);


        return  dishDtoPage;
    }

    @Override
    public DishDto findById(Long id) {
        Dish dish=dishMapper.findById(id);
        List<DishFlavor> dishFlavorList=dishFlavorMapper.findByDishId(id);
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish,dishDto);
        dishDto.setFlavors(dishFlavorList);
        return dishDto;
    }

    @Override
    @Transactional
    public void updateWithFlavor(DishDto dishDto) {
        dishDto.setUpdateTime(LocalDateTime.now());
        dishMapper.updateById(dishDto);
        dishFlavorMapper.deleteByDishId(dishDto.getId());
        List<DishFlavor> flavors = dishDto.getFlavors();
        for (DishFlavor flavor : flavors) {
            flavor.setDishId(dishDto.getId());
            flavor.setCreateTime(LocalDateTime.now());
            flavor.setUpdateTime(LocalDateTime.now());
            flavor.setCreateUser(dishDto.getCreateUser());
            flavor.setUpdateUser(dishDto.getUpdateUser());
        }
        dishFlavorMapper.saveBatch(flavors);
        Set keys = redisTemplate.keys("dish_*");
        redisTemplate.delete(keys);
    }
    /*
     * 方法作用： 根据菜品类别的id查找的菜品
     */
    @Override
    public List<DishDto> findByCategoryId(Long categoryId,Integer status) {
        List<DishDto> dishDtoList=null;
        //先查询redis,查看redis,查看redis是否存在点击类别的菜品如果有缓存拿出
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String key="dish_"+categoryId+"_"+status;
        dishDtoList=(List<DishDto>)valueOperations.get(key);
        if (dishDtoList==null){
            dishDtoList=new ArrayList<>();;
            List<Dish> dishList = dishMapper.findByCategoryId(categoryId,status);
            for (Dish dish : dishList) {
                DishDto dishDto = new DishDto();
                BeanUtils.copyProperties(dish,dishDto);
                List<DishFlavor> dishFlavorList=dishFlavorMapper.findByDishId(dish.getId());
                dishDto.setFlavors(dishFlavorList);
                Category category=cateGoryMapper.findById(dish.getCategoryId());
                dishDto.setCategoryName(category.getName());
                dishDtoList.add(dishDto);
            }
        }
         valueOperations.set(key,dishDtoList,60*24,TimeUnit.MINUTES);

        return dishDtoList;
    }
   //批量删除菜品
    @Override
    public void deletDishById(List<Long> ids) {
        //查询菜品是否有起售的
      Long count=dishMapper.findDishCount(ids);
        if(count>0){
            throw  new CustomException("存在启售，无法删除");
        }


       List<SetmealDish> setmealDish=setmealDishMapper.findmealDish(ids);
        for (SetmealDish dish : setmealDish) {
            if (dish.getSetmealId()!=0){
                throw  new CustomException("存在套餐，无法删除");
            }
        }

        dishMapper.deleteDishById(ids);
        dishFlavorMapper.deleteDishByidFlavor(ids);
    }

    @Override
    public void updateStar(Integer status, List<Long> ids) {
        dishMapper.updateStar(status,ids);
    }
}
