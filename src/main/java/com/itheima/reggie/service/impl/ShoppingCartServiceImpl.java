package com.itheima.reggie.service.impl;

import com.itheima.reggie.commom.R;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.mapper.ShoppingCartMapper;
import com.itheima.reggie.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {
    @Autowired(required = false)
    private ShoppingCartMapper shoppingCartMapper;
//添加到购物车单个商品
    @Override
    public ShoppingCart add(ShoppingCart shoppingCart) {
        ShoppingCart shoppingCartOne = shoppingCartMapper.findUidAndDidOrSid(shoppingCart);
        if (shoppingCartOne == null) {
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartMapper.save(shoppingCart);
            shoppingCartOne = shoppingCart;
        } else
            shoppingCartOne.setNumber(shoppingCartOne.getNumber() + 1);
        shoppingCartMapper.update(shoppingCartOne);


        return shoppingCartOne;
    }
    //删除购物车单个商品
    @Override
    public void singleClean(Long user, ShoppingCart shoppingCart) {
            shoppingCart.setUserId(user);
        ShoppingCart shoppingCartOne = shoppingCartMapper.findUidAndDidOrSid(shoppingCart);
        Integer number = shoppingCartOne.getNumber();
        System.out.println(number);
        if (number > 1) {
            shoppingCartOne.setNumber(shoppingCartOne.getNumber() - 1);
            shoppingCartMapper.update(shoppingCartOne);

        } else {
            shoppingCartMapper.clean(user);

        }

    }

    @Override
    public List<ShoppingCart> findCartByUserId(Long user) {
        List<ShoppingCart> shoppingCarts = shoppingCartMapper.findcartByUserId(user);
        return shoppingCarts;
    }

    @Override
    public void clean(Long user) {
        shoppingCartMapper.clean(user);
    }



}


