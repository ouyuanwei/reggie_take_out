package com.itheima.reggie.service;

import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Page;

import java.util.List;

public interface DishService {
    void saveWithFlavor(DishDto dishDto);

    Page<DishDto> page(Integer page, Integer pageSize, String name);

    DishDto findById(Long id);

    void updateWithFlavor(DishDto dishDto);

    List<DishDto> findByCategoryId(Long categoryId,Integer status);


    void deletDishById(List<Long> ids);

    void updateStar(Integer status, List<Long> ids);
}
