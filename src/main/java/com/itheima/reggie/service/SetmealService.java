package com.itheima.reggie.service;

import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.entity.Setmeal;

import java.util.List;

public interface SetmealService {
    void save(SetmealDto setmealDto);

    Page<SetmealDto> findByPage(Integer page, Integer pageSize, String name);

    void deleteById(List<Long> ids);

    List<Setmeal> list(Long categoryId, Integer status);

    void updateSetmealStart(Integer status, List<Long> ids);


    SetmealDto updatefind(Long id);

    void updatefind2(SetmealDto setmealDto);

}
