package com.itheima.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.itheima.reggie.commom.R;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebFilter(urlPatterns = "/*")
/*@RestController*/
public class LoginCheckFilter implements Filter {
    // 该类的作用主要用于匹配url地址
   private AntPathMatcher antPathMatcher = new AntPathMatcher();
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
     HttpServletRequest request=(HttpServletRequest) servletRequest;
     HttpServletResponse response=(HttpServletResponse) servletResponse;
        String requestURI = request.getRequestURI();
        String[] urls={ "/employee/login","/backend/**","/front/**","/common/**","/user/sendMsg","/user/login"};
        boolean flag=cherkUri(urls,requestURI);

        if(flag){
            filterChain.doFilter(request, response);
            return;
        }

        Object employee = request.getSession().getAttribute("employee");

        if(employee!=null){
            filterChain.doFilter(request, response);
            return;
        }

        //前台页面的登陆校验
        Object user = request.getSession().getAttribute("user");
        if(user!=null){
            //用户已经登陆
            filterChain.doFilter(request,response);
            return;
        }

        String  resultJson= JSON.toJSONString(R.error("NOTLOGIN"));
        response.getWriter().write(resultJson);
        System.out.println("拦截了");
    }



    private boolean cherkUri(String[] urls, String requestURI) {
        for (String url : urls) {
         if(antPathMatcher.match(url,requestURI)){
                return true;
         }
        }
        return false;
    }
}
