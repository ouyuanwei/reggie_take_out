package com.itheima.reggie.mapper;

import com.itheima.reggie.entity.ShoppingCart;
import org.apache.ibatis.annotations.*;

import java.util.List;


public interface ShoppingCartMapper  {
    //添加购物车
    @Insert("insert into shopping_cart values(null,#{name},#{image},#{userId},#{dishId},#{setmealId},#{dishFlavor},#{number},#{amount},#{createTime})")
    void save(ShoppingCart shoppingCart);
    //修改购物车份数
    @Update("update shopping_cart set number=#{number} where id=#{id}")
    void update(ShoppingCart shoppingCartOne);

    ShoppingCart findUidAndDidOrSid(ShoppingCart shoppingCart);
    //根据用户id查看购物车
    @Select("select * from shopping_cart where user_id=#{userId}")
    List<ShoppingCart> findcartByUserId(Long user);
   //购物车全部清除
   @Delete("delete from shopping_cart where user_id=#{userId}")
    void clean(Long user);


}