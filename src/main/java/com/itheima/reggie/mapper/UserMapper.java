package com.itheima.reggie.mapper;

import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper {
   @Select("select * from user where phone=#{phone}")
    User findByPhone(String phone);

    @Insert("insert into user(phone,status) values(#{phone},#{status})")
    @Options(useGeneratedKeys = true,keyColumn = "id",keyProperty = "id")
    void save(User dbUser);

 @Select("select * from user where id=#{userId}")
    User findById(Long user);



}
