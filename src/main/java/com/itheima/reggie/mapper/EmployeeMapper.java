package com.itheima.reggie.mapper;

import com.itheima.reggie.entity.Employee;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;

public interface EmployeeMapper {
   @Select("select * from employee where username=#{username}")
    Employee login(Employee employee);
   @Insert("insert into employee values(null,#{name},#{username},#{password},#{phone},#{sex},#{idNumber},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void save(Employee employee);

    List<Employee> findByName(@Param("name") String name);

    void update(Employee employee);
  @Select("select * from employee where id=#{id}")
    Employee findById(long id);
}

