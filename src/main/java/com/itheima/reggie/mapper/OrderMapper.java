package com.itheima.reggie.mapper;

import com.itheima.reggie.entity.Orders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrderMapper {
    @Insert("insert into orders values(#{id},#{number},#{status},#{userId},#{addressBookId},#{orderTime},#{checkoutTime},#{payMethod},#{amount},#{remark},#{phone},#{address},#{userName},#{consignee})")
    void save(Orders orders);
    @Select("SELECT * FROM orders WHERE user_id=#{userId}")
    List<Orders> findNewOrder(Long user);

    List<Orders> findordername(@Param("number") String number,@Param("beginTime")String beginTime, @Param("endTime")String endTime);
}
