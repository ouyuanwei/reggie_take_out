package com.itheima.reggie.mapper;

import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SetMealMapper {
@Select("select count(*) from setmeal where category_id=#{id}")
    long findSetMealByCategoryId(Long id);
    @Insert("insert into setmeal values(null,#{categoryId},#{name},#{price},#{status},#{code},#{description},#{image},#{createTime},#{updateTime},#{createUser},#{updateUser},0)")
    @Options(useGeneratedKeys = true,keyProperty = "id",keyColumn = "id")
    void save(SetmealDto setmealDto);

    List<Setmeal> findByname(@Param("name") String name);

    Long queryDishWithStatus( @Param("ids") List<Long> ids);

    void deletByIds(@Param("ids") List<Long> ids);

    //通过套餐id查询套餐菜品的表
    /*void findSetMealDish(List<Long> ids);*/

    void updateSetmealStart(Integer status, List<Long> ids);
    @Select("select * from setmeal where id=#{id}")
    Setmeal findsetmeal(Long id);

    void updatefind2(SetmealDto setmealDto);
}
