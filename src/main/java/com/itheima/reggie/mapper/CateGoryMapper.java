package com.itheima.reggie.mapper;

import com.itheima.reggie.entity.Category;
import org.apache.ibatis.annotations.*;

import java.util.List;


public interface CateGoryMapper {
    @Insert("INSERT INTO category values(NULL,#{type},#{name},#{sort},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void save(Category category);

    @Select("select * from category order by sort")
    List<Category> findAll();

     @Delete("delete from category where id=#{id}")
    void deletById(long id);
    @Update("update category set type=#{type},name=#{name},sort=#{sort},update_time=#{updateTime},update_user=#{updateUser} where id=#{id}")
    void updateById(Category category);
/*    @Select("select * from category where type=#{type}")*/
    List<Category> list(@Param("type") Integer type);
   @Select("select * from category where id=#{id}")
    Category findById(Long categoryId);
}
