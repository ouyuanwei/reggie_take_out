package com.itheima.reggie.mapper;

import com.itheima.reggie.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DishFlavorMapper {
    void saveBatch(@Param("flavors") List<DishFlavor> flavors);
    @Select("select * from dish_flavor where dish_id=#{dishId}")
    List<DishFlavor> findByDishId(Long id);
    @Delete("delete from dish_flavor where dish_id=#{dishId}")


    void deleteByDishId(Long id);


    void deleteDishByidFlavor(@Param("ids") List<Long> ids);
}
