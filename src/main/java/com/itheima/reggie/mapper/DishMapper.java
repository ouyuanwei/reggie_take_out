package com.itheima.reggie.mapper;

import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DishMapper {

    @Select("SELECT COUNT(*) FROM dish WHERE category_id=#{categoryId}")
    long findDishCountByCategoryId(long id);

    //通知mybatis插入数据之后，把id列的值设置给实体类的id属性
    @Insert("insert into  dish values(null,#{name},#{categoryId},#{price},#{code},#{image},#{description},#{status},#{sort},#{createTime},#{updateTime},#{createUser},#{updateUser},0)")
    @Options(useGeneratedKeys=true,keyColumn = "id",keyProperty = "id")
    void save(DishDto dishDto);

    List<Dish> findByName(String name);
    @Select("select * from dish where id=#{id}")
    Dish findById(Long id);

    void updateById(DishDto dishDto);

    //菜品查询
    /*@Select("select * from dish where category_id=#{categoryId} and status=#{status} ")*/
    List<Dish> findByCategoryId(@Param("categoryId") Long categoryId,@Param("status") Integer status);
    //查询菜品是否有起售的
    Long findDishCount(@Param("ids") List<Long> ids);


    void deleteDishById(@Param("ids") List<Long> ids);

    void updateStar(Integer status, @Param("ids") List<Long> ids);
}
