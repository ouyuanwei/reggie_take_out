package com.itheima.reggie.mapper;

import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SetmealDishMapper {
    void saveBatch(@Param("setmealDishes") List<SetmealDish> setmealDishes);

    void deleteBySetmealId(@Param("ids") List<Long> ids);
    /*
     * 根据套餐的类别展示套餐
     */
    @Select("select * from setmeal where category_id=#{categoryId} and status=#{status}")
    List<Setmeal> list(@Param("categoryId") Long categoryId,@Param("status") Integer status);


    List<SetmealDish> findmealDish(@Param("ids") List<Long> ids);
    @Select("select * from setmeal_dish where setmeal_id=#{id}")
    List<SetmealDish> finsetmealdish(Long id);
    @Delete("delete from setmeal_dish where id=#{id}")
    void deletedishone(Long id);
}
