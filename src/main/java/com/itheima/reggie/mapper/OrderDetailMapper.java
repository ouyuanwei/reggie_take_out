package com.itheima.reggie.mapper;

import com.itheima.reggie.entity.OrderDetail;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrderDetailMapper {
    void saverBatch(@Param("orderDetails") List<OrderDetail> orderDetails);
    @Select("select * from order_detail where order_id=#{orderId}")
    List<OrderDetail> findNewOrderDetail(Long id);
}
