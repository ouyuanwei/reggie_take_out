package com.itheima.reggie.commom;

public class CustomException extends RuntimeException {
    public CustomException(String message) {
        super(message);
    }
}
