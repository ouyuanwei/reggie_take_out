package com.itheima.reggie.commom;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice(annotations = {Controller.class, RestController.class})
@ResponseBody
@Slf4j
public class GlobalExceptionHandle {
   @ExceptionHandler(value = Exception.class)
    public R<String> handlerException(Exception e){
       log.info("异常处理");
       e.printStackTrace();
       return R.error("服务器异常"+e.getMessage());
   }


}
