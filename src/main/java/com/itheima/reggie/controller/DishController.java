package com.itheima.reggie.controller;

import com.itheima.reggie.commom.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {
    @Autowired
    private DishService dishService;
    @Autowired
    private DishFlavorService dishFlavorService;

    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto, HttpSession session){
        Long employee =(Long) session.getAttribute("employee");
        dishDto.setCreateUser(employee);
        dishDto.setUpdateUser(employee);
        dishService.saveWithFlavor(dishDto);
        return R.success("保存菜品成功");
    }


    @GetMapping("/page")
    public R<Page<DishDto>> page(Integer page, Integer pageSize, String name) {
        Page<DishDto> pageResult =  dishService.page(page,pageSize,name);
        return R.success(pageResult);
    }

    @GetMapping("/{id}")
    public R<DishDto> get(@PathVariable("id") Long id){
                 DishDto dishDto=dishService.findById(id);
                 return R.success(dishDto);
    }

    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto,HttpSession session){
        Long employee =(Long) session.getAttribute("employee");
        dishDto.setUpdateUser(employee);
         dishService.updateWithFlavor(dishDto);
         return  R.success("修改菜品成功");
    }
    /*
     * 方法作用： 根据菜品类别的id查找的菜品
     */
  @GetMapping("list")
    public R<List<DishDto>> list(Long categoryId,Integer status){
      List<DishDto> dishList=dishService.findByCategoryId(categoryId,status);
      return R.success(dishList);
    }
   //
    @DeleteMapping
    public R<String> deleteDish(@RequestParam List<Long> ids) {
        dishService.deletDishById(ids);
        return R.success("删除成功");
    }


    @PostMapping("/status/{status}")
    public R<String> update(@PathVariable Integer status,@RequestParam List<Long> ids){
          dishService.updateStar(status,ids);
          return R.success("修改成功");
    }

    }


