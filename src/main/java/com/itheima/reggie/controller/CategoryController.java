package com.itheima.reggie.controller;

import com.itheima.reggie.commom.R;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
   private CategoryService categoryService;
    @PostMapping
    public R<String> save(@RequestBody Category category, HttpSession session){
        Long empId=(Long)session.getAttribute("employee");
        category.setCreateTime(LocalDateTime.now());
        category.setUpdateTime(LocalDateTime.now());
        category.setCreateUser(empId);
        category.setUpdateUser(empId);
        categoryService.sava(category);
        return R.success("新增分类成功");
    }

    @GetMapping("/page")
    public R<Page<Category>> findByPage(Integer page,Integer pageSize){
             Page<Category> pageResult=categoryService.findByPage(page,pageSize);
             return  R.success(pageResult);
    }
   @DeleteMapping
    public R<String> delete(long id){
        categoryService.removeBtId(id);
        return  R.success("分类信息删除成功");
   }

   @PutMapping
    public R<String> update(@RequestBody Category category,HttpSession session){
      Long employee=(Long)session.getAttribute("employee");
       category.setUpdateUser(employee);
       categoryService.updateById(category);

       return R.success("修改成功");

    }

    @GetMapping("/list")
    public  R<List<Category>> list(Integer type){
        List<Category> categoryList=categoryService.list(type);
                return R.success(categoryList);
    }


}
