package com.itheima.reggie.controller;

import com.itheima.reggie.commom.R;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 购物车
 */
@Slf4j
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;
//添加购物车
    @PostMapping("add")
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart, HttpSession session) {
        Long user = (Long) session.getAttribute("user");
        shoppingCart.setUserId(user);
        ShoppingCart shoppingCartR = shoppingCartService.add(shoppingCart);
        return R.success(shoppingCartR);
    }
    //查询购物车
@GetMapping("list")
    public R<List<ShoppingCart>> list(HttpSession session){
    Long user =(Long)session.getAttribute("user");
    List<ShoppingCart> shoppingCarts=shoppingCartService.findCartByUserId(user);
    return R.success(shoppingCarts);
}
//删除购物车所有
    @DeleteMapping("/clean")
    public R<String> clean(HttpSession session){
        Long user =(Long) session.getAttribute("user");
        shoppingCartService.clean(user);
        return  R.success("清空购物车成功");
    }
    //删除购物车单个商品(实战)
    @PostMapping("/sub")
    public R<String> singleClean(@RequestBody ShoppingCart shoppingCart,HttpSession session){
        Long user =(Long) session.getAttribute("user");
        shoppingCartService.singleClean(user,shoppingCart);
        return  R.success("删除成功");
    }

}