package com.itheima.reggie.controller;

import com.itheima.reggie.commom.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.OrderDto;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {
    @Autowired(required = false)
    private OrderService orderService;
    @PostMapping("/submit")
    public R<String> submit(HttpSession session, @RequestBody Orders orders){
        Long user =(Long) session.getAttribute("user");
               orderService.submit(orders,user);
              return R.success("下达成功");
    }
    //分页查询历史订单和最新订单（实战）
    @GetMapping("/userPage")
    public R<Page<OrderDto>> page(Integer page, Integer pageSize, HttpSession session) {
        Long user = (Long) session.getAttribute("user");
        Page<OrderDto> pageResult =  orderService.page(page,pageSize,user);
        return R.success(pageResult);
    }

    @GetMapping("/page")
    public R<Page<OrderDto>> page(Integer page, Integer pageSize, String number,String beginTime,String endTime) {
        Page<OrderDto> pageResult = orderService.page2(page,pageSize,number,beginTime,endTime);
        return R.success(pageResult);
    }
}
