package com.itheima.reggie.controller;

import com.itheima.reggie.commom.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.OrderDto;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.service.UserService;
import com.itheima.reggie.util.SMSUtils;
import com.itheima.reggie.util.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Autowired(required = false)
    private UserService userService;
    @Autowired(required = false)
     private RedisTemplate redisTemplate;

    @RequestMapping("sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession session){
        String phone = user.getPhone();
        String s = ValidateCodeUtils.generateValidateCode(4).toString();
        //SMSUtils.sendMessage("黑马旅游网","SMS_205126318",phone,s);
        log.info(s);
        //session.setAttribute(phone,s);
        redisTemplate.opsForValue().set(phone,s,1, TimeUnit.MINUTES);
        return R.success("发送成功");
    }

    @RequestMapping("login")
    public R<User> login(@RequestBody Map<String,String> param, HttpSession session){
        String phone = param.get("phone");
        String code = param.get("code");
        //String codeInSession =(String) session.getAttribute(phone);
        String codeInSession =(String) redisTemplate.opsForValue().get(phone);
        User user=userService.login(phone,code,codeInSession);
                 if(user!=null){
                     session.setAttribute("user",user.getId());
                     redisTemplate.delete(phone);
                 }else {
                     return  R.error("登录失败");
                 }
                 return R.success(user);
    }
    //实战退出
    @PostMapping("/loginout")
    public R<String> logout( HttpSession sesission){
        sesission.invalidate(); //清除session
        return R.success("退出成功");
    }






    }


