package com.itheima.reggie.controller;

import com.itheima.reggie.commom.R;

import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/employee")
@Slf4j
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;



    @PostMapping("/login")
    public R<Employee> login(@RequestBody Employee employee, HttpSession session) {
        /* 把参数传给service*/
        R<Employee> result = employeeService.login(employee);
        /*判断是登录成功*/
        if (result.getCode() == 1) {
            //设置登录成功标记
            session.setAttribute("employee", result.getData().getId());
        }
        return result;
    }

    @PostMapping("/logout")
    public R<String> logout(HttpSession sesission) {
        sesission.invalidate(); //清除session
        return R.success("退出成功");
    }


    @PostMapping
    public R<String> save(@RequestBody Employee employee, HttpSession session) {
        Long employee1 = (Long) session.getAttribute("employee");
        employee.setCreateUser(employee1);
        employee.setUpdateUser(employee1);
        employeeService.save(employee);
        return R.success("添加成功");

    }

    @GetMapping("/page")
    public R<Page<Employee>> page(Integer page, Integer pageSize, String name) {
        Page<Employee> pageResult = employeeService.findByPage(page, pageSize, name);
        return R.success(pageResult);
    }
    @PutMapping
    public R<String> update(@RequestBody Employee employee,HttpSession session){
        Long employee1 =(Long)session.getAttribute("employee");
        employee.setUpdateUser(employee1);
         employeeService.update(employee);
         return R.success("修改成功");
    }


    @GetMapping("/{id}")
   public  R<Employee> findById(@PathVariable("id") long id){
        Employee employee=employeeService.findById(id);
        return R.success(employee);
    }
}
