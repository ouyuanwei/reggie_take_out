package com.itheima.reggie.controller;

import com.itheima.reggie.commom.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Page;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/setmeal")
public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    @PostMapping
    public R<String> save(@RequestBody SetmealDto setmealDto, HttpSession session){
       long empId=(Long) session.getAttribute("employee");
       setmealDto.setCreateUser(empId);
       setmealDto.setUpdateUser(empId);
       setmealService.save(setmealDto);
       return R.success("保存成功");
    }

    @GetMapping("/page")
    public R<Page<SetmealDto>> page(Integer page,Integer pageSize,String name){
        Page<SetmealDto> pageResult=setmealService.findByPage(page,pageSize,name);
        log.info(name+"==================================");
        return  R.success(pageResult);
    }

    @DeleteMapping
    public R<String> delete(@RequestParam List<Long> ids) {
                     setmealService.deleteById(ids);
                     return R.success("删除成功");
    }
    /*
     * 根据套餐的类别展示套餐
     */
    @GetMapping("/list")
    public R<List<Setmeal>> list(Long categoryId, Integer status) {
        List<Setmeal> setmealList =   setmealService.list(categoryId,status);
        return R.success(setmealList);
    }

    @PostMapping("/status/{status}")
    public R<String> updateSetmealStart(@PathVariable Integer status,@RequestParam List<Long> ids){
        setmealService.updateSetmealStart(status,ids);
        return R.success("修改成功");
    }

    @GetMapping("/{id}")
    public R<SetmealDto> updatefind( @PathVariable("id") Long id){
           SetmealDto setmealDto=setmealService.updatefind(id);
             return  R.success(setmealDto);
    }
    @PutMapping
    public R<String> updatefind2(@RequestBody SetmealDto setmealDto, HttpSession session){
        Long employee = (Long) session.getAttribute("employee");
          setmealDto.setUpdateUser(employee);
          setmealDto.setUpdateTime(LocalDateTime.now());
           setmealService.updatefind2(setmealDto);
           return R.success("修改成功");
    }
}
