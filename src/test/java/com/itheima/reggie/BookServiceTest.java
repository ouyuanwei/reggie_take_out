package com.itheima.reggie;


import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.mapper.ShoppingCartMapper;

import com.itheima.reggie.service.ShoppingCartService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

//可以指定启动类的名字，测试类中启动类中同一级或它的子包下，则可以省略
@SpringBootTest(classes = Application.class)
public class BookServiceTest {

    @Autowired(required = false)
    private ShoppingCartService shoppingCartService;
    @Autowired(required = false)
    private ShoppingCartMapper   shoppingCartMapper;

    @Test
    void testSave() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setDishId(1397849739276890114L);
        shoppingCartMapper.findUidAndDidOrSid(shoppingCart);
    }
}
